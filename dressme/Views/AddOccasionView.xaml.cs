﻿using dressme.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace dressme.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddOccasionView : Page
    {
        public AddOccasionView()
        {
            this.InitializeComponent();
        }

        //Navigate to looks setup screen
        private void AddItemBtn_Click(object sender, RoutedEventArgs e)
        {
            OccasionModel occa = new OccasionModel();
            occa.name = textBox_name.Text;
            occa.desc = textBox_desc.Text;
            //occa.mood = Convert.ToInt16(textBox_mood.Text);
            occa.mood = 0;
            occa.location = textBox_location.Text;
            occa.date = dtpicker.Date.ToString();
            occa.url = textBox_url.Text;
            occa.picture = "";

            //Redirect to Setup Looks screen
            this.Frame.Navigate(typeof(SetupLooksScreen), occa);
        }

        //Get picture grom device camera
        private async void getPictureFromCamera(object sender, RoutedEventArgs e)
        {
            //Show loading
            LoadingIndicator.IsActive = true;

            CameraCaptureUI capture = new CameraCaptureUI();
            capture.PhotoSettings.Format = CameraCaptureUIPhotoFormat.Jpeg;
            capture.PhotoSettings.CroppedAspectRatio = new Size(3, 5);
            capture.PhotoSettings.MaxResolution = CameraCaptureUIMaxPhotoResolution.HighestAvailable;
            StorageFile file = await capture.CaptureFileAsync(CameraCaptureUIMode.Photo);
            //Add look image to show on screen
            addLook(file);
        }

        //Get picture from the device gallery
        private async void getPictureFromGallery(object sender, RoutedEventArgs e)
        {
            //Show loading
            LoadingIndicator.IsActive = true;

            //Open Library or Camera and get the Image
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");

            StorageFile file = await openPicker.PickSingleFileAsync();
            //Add look image to show on screen
            addLook(file);
        }


        //Add new look to be saved further
        private async void addLook(StorageFile file)
        {
            try
            {
                if (file != null)
                {
                    //Add the image to the screen grid
                    BitmapImage bitmapimage = new BitmapImage();
                    var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    await bitmapimage.SetSourceAsync(stream);
                    imgBanner.Source = bitmapimage;
                    //lookImage.VerticalAlignment = VerticalAlignment.Top;
                }
            }
            finally
            {
                LoadingIndicator.IsActive = false;
            }
        }

    }
}
