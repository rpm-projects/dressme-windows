﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using winsdkfb;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace dressme.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SignupView : Page 
    {
        public SignupView()
        {
            this.InitializeComponent();
        }

        //Create new account
        private async void createAccount(object sender, RoutedEventArgs e)
        {
            HttpClient client = new HttpClient();

            try
            {
                Uri uri = new Uri("https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyCvdfTBL_rlqnaLZW8mfQYdjDVwq4-WOC8");

                JsonObject jsonObject = new JsonObject();
                jsonObject.Add("email", JsonValue.CreateStringValue(textBox_username.Text));
                jsonObject.Add("password", JsonValue.CreateStringValue(textBox_password.Password.ToString()));
                jsonObject.Add("returnSecureToken", JsonValue.CreateBooleanValue(true));

                HttpStringContent stringContent = new HttpStringContent(jsonObject.Stringify()
                  ,
                  Windows.Storage.Streams.UnicodeEncoding.Utf8,
                  "application/json");

                client.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsync(
                    uri,
                    stringContent);

                Debug.WriteLine(response);

                string alertMessage = "";

                if (response.IsSuccessStatusCode)
                {
                    //Redirect to login screen
                    this.Frame.Navigate(typeof(LoginView));
                    Debug.WriteLine("Authenticated");
                    alertMessage = "Successfully created!";
                }
                else
                {
                    //TODO show alert of error
                    Debug.WriteLine("Failed to create new user!");
                    alertMessage = "There was a problem!";
                }

                string title = "New Account";
                ToastNotifier ToastNotifier = ToastNotificationManager.CreateToastNotifier();
                Windows.Data.Xml.Dom.XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
                Windows.Data.Xml.Dom.XmlNodeList toastNodeList = toastXml.GetElementsByTagName("text");
                toastNodeList.Item(0).AppendChild(toastXml.CreateTextNode(title));
                toastNodeList.Item(1).AppendChild(toastXml.CreateTextNode(alertMessage));
                Windows.Data.Xml.Dom.IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                Windows.Data.Xml.Dom.XmlElement audio = toastXml.CreateElement("audio");
                audio.SetAttribute("src", "ms-winsoundevent:Notification.SMS");

                ToastNotification toast = new ToastNotification(toastXml);
                toast.ExpirationTime = DateTime.Now.AddSeconds(4);
                ToastNotifier.Show(toast);
            }
            finally
            {
                client.Dispose();
            }

        }
    }
}
