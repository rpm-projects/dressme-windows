﻿using dressme.Config;
using dressme.Models;
using System;
using System.Collections.Generic;
using Windows.Web.Http;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.Data.Json;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Media.Capture;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace dressme.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SetupLooksScreen : Page
    {

        private OccasionModel occa;
        private List<LookModel> looksList = new List<LookModel>();
        private int gridCol = -1;
        private int counterUpl = 0;

        public SetupLooksScreen()
        {
            this.InitializeComponent();
        }

        //Receive obj occasion from previous screen
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.occa = e.Parameter as OccasionModel;
        }

        //Save o occasion(with looks) and start looks images upload
        private async void save(object sender, RoutedEventArgs e)
        {
            HttpClient client = new HttpClient();

            try
            {
                LoadingIndicator.IsActive = true;

                Uri uri = new Uri(APIConfig.URL_occasions);

                Windows.Data.Json.JsonObject jsonObject = new Windows.Data.Json.JsonObject();

                //Special parameter to tell it comes from a Win App then it will be possivle to normalize the looks data
                jsonObject.Add("win_app", JsonValue.CreateBooleanValue(true));

                jsonObject.Add("name", JsonValue.CreateStringValue(occa.name ?? ""));
                jsonObject.Add("desc", JsonValue.CreateStringValue(occa.desc ?? ""));
                jsonObject.Add("mood", JsonValue.CreateStringValue(occa.mood.ToString() ?? ""));
                jsonObject.Add("location", JsonValue.CreateStringValue(occa.location ?? ""));
                jsonObject.Add("date", JsonValue.CreateStringValue(occa.date ?? ""));
                jsonObject.Add("time", JsonValue.CreateStringValue(occa.time ?? ""));
                jsonObject.Add("url", JsonValue.CreateStringValue(occa.url ?? ""));
                jsonObject.Add("picture", JsonValue.CreateStringValue(occa.picture ?? ""));

                //Create list of looks json object to make post request further
                JsonArray jsonObjLooksList = new JsonArray();

                foreach (LookModel look in looksList)
                {
                    JsonObject jsonObjLook = new JsonObject();
                    jsonObjLook.Add("desc", JsonValue.CreateStringValue(look.desc ?? ""));
                    jsonObjLook.Add("dislike", JsonValue.CreateNumberValue(0));
                    jsonObjLook.Add("like", JsonValue.CreateNumberValue(0));
                    jsonObjLook.Add("picture", JsonValue.CreateStringValue(""));
                    jsonObjLook.Add("idForUpload", JsonValue.CreateStringValue(look.idForUpload));

                    string jsonStr = jsonObjLook.Stringify();

                    jsonObjLooksList.Add(JsonValue.CreateStringValue(jsonStr));
                }

                string jsonStrLooks = jsonObjLooksList.Stringify();
                jsonObject.Add("looks", JsonValue.CreateStringValue(jsonStrLooks));

                HttpStringContent stringContent = new HttpStringContent(
                  jsonObject.Stringify(),
                  Windows.Storage.Streams.UnicodeEncoding.Utf8,
                  "application/json");

                //[POST OCCASION WITH LOOKS]
                client.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsync(
                    uri,
                    stringContent);

                JsonObject jsonResp = JsonObject.Parse(response.Content.ToString());
                string idOccasion = jsonResp.GetNamedString("id");
                JsonArray respLooks = jsonResp.GetNamedArray("looks");

                //Get response from provious POST and upload Looks images base on Look id
                for (int i = 0; i < respLooks.Count; i++)
                {
                    JsonObject respJsonLook = respLooks[i].GetObject();

                    //Upload images base on look id
                    string idLook = respJsonLook.GetNamedString("id");
                    string idForUpload = respJsonLook.GetNamedString("idForUpload");
                    string imageUplName = "occasion_" + idOccasion + "_look_" + idLook;

                    //get the proper Storage File
                    for (int j = 0; j < looksList.Count; j++)
                    {
                        if (looksList[j].idForUpload == idForUpload)
                        {
                            uploadImage(looksList[j].storageFile, imageUplName);
                        }
                    }
                }

                client.Dispose();
            }
            finally
            {
                client.Dispose();
            }
        }

        //Get picture grom device camera
        private async void getPictureFromCamera(object sender, RoutedEventArgs e)
        {
            //Show loading
            LoadingIndicator.IsActive = true;

            CameraCaptureUI capture = new CameraCaptureUI();
            capture.PhotoSettings.Format = CameraCaptureUIPhotoFormat.Jpeg;
            capture.PhotoSettings.CroppedAspectRatio = new Size(3, 5);
            capture.PhotoSettings.MaxResolution = CameraCaptureUIMaxPhotoResolution.HighestAvailable;
            StorageFile file = await capture.CaptureFileAsync(CameraCaptureUIMode.Photo);
            //Add look image to show on screen
            addLook(file);
        }

        //Get picture from the device gallery
        private async void getPictureFromGallery(object sender, RoutedEventArgs e)
        {
            //Show loading
            LoadingIndicator.IsActive = true;

            //Open Library or Camera and get the Image
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");

            StorageFile file = await openPicker.PickSingleFileAsync();
            //Add look image to show on screen
            addLook(file);
        }


        //Add new look to be saved further
        private async void addLook(StorageFile file)
        {
            try
            {
                if (file != null)
                {
                    //Add the image to the screen grid
                    Image lookImage = new Image();
                    BitmapImage bitmapimage = new BitmapImage();
                    var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    await bitmapimage.SetSourceAsync(stream);
                    lookImage.Source = bitmapimage;
                    lookImage.VerticalAlignment = VerticalAlignment.Top;
                    gridCol++;
                    lookImage.SetValue(Grid.ColumnProperty, gridCol);
                    
                    GridLooks.Children.Add(lookImage);

                    //Add a new look obj to a list
                    LookModel look = new LookModel();
                    look.storageFile = file; //store the image
                    look.desc = "";
                    look.dislike = 0;
                    look.like = 0;
                    look.picture = "";
                    look.idForUpload = "id_tmp_" + DateTime.Now;

                    looksList.Add(look);
                }
            }
            finally
            {
                LoadingIndicator.IsActive = false;
            }
        }

        public async void uploadImage(StorageFile storageFile, string imageUplName)
        {
            HttpClient client = new HttpClient();

            try
            {
                LoadingIndicator.IsActive = true;

                if (storageFile != null)
                {
                    //Convert image to string base64
                    IRandomAccessStream fileStream = await storageFile.OpenAsync(FileAccessMode.Read);
                    var reader = new DataReader(fileStream.GetInputStreamAt(0));
                    await reader.LoadAsync((uint)fileStream.Size);
                    byte[] byteArray = new byte[fileStream.Size];
                    reader.ReadBytes(byteArray);
                    string base64String = Convert.ToBase64String(byteArray);

                    //Set upload look URL
                    Uri uri = new Uri(APIConfig.URL_upload_look);

                    Windows.Data.Json.JsonObject jsonObject = new Windows.Data.Json.JsonObject();
                    jsonObject.Add("file", JsonValue.CreateStringValue(base64String));
                    jsonObject.Add("name", JsonValue.CreateStringValue(imageUplName));

                    HttpStringContent stringContent = new HttpStringContent(
                      jsonObject.Stringify(),
                      Windows.Storage.Streams.UnicodeEncoding.Utf8,
                      "application/json");

                    client.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.PostAsync(
                        uri,
                        stringContent);

                    Debug.WriteLine(response);

                    client.Dispose();

                    string alertMessage = "";

                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("Image Upload OK");
                    }
                    else
                    {
                        Debug.WriteLine("Image Upload O Failed!");
                    }

                    counterUpl++;
                    if(counterUpl == looksList.Count)
                    {
                        //Hide loading
                        LoadingIndicator.IsActive = false;

                        //Redirect to Main screen
                        this.Frame.Navigate(typeof(MainPage));
                    }
                }
            }
            finally
            {
                client.Dispose();
                LoadingIndicator.IsActive = false;
                //Redirect to Main screen
                this.Frame.Navigate(typeof(MainPage));
            }
        }
    }
}
