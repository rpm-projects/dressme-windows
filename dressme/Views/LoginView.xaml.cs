﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using winsdkfb;
using System.Diagnostics;
using Windows.Web.Http;
using Windows.UI.Notifications;
using Windows.Data.Json;
using Chilkat;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using dressme.Config;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace dressme.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView : Page
    {

        private readonly string[] requested_permissions =
        {
            "public_profile",
            "email",
            "user_friends",
            "publish_actions"
        };

        public LoginView()
        {
            this.InitializeComponent();
        }

        //Default login
        private async void authenticate(object sender, RoutedEventArgs e)
        {

            Uri uri = new Uri(APIConfig.URL_DEFAULT_AUTHENTICATION);

            Windows.Data.Json.JsonObject jsonObject = new Windows.Data.Json.JsonObject();
            jsonObject.Add("email", JsonValue.CreateStringValue(textBox_username.Text));
            jsonObject.Add("password", JsonValue.CreateStringValue(textBox_password.Password.ToString()));
            jsonObject.Add("returnSecureToken", JsonValue.CreateBooleanValue(true));

            HttpStringContent stringContent = new HttpStringContent(
              jsonObject.Stringify(),
              Windows.Storage.Streams.UnicodeEncoding.Utf8,
              "application/json");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.PostAsync(
                uri,
                stringContent);

            Debug.WriteLine(response);

            client.Dispose();

            if (response.IsSuccessStatusCode)
            {
                    //Redirect to home screen
                    this.Frame.Navigate(typeof(MainPage));
            }
            else
            {
                string title = "Auhtentication Problem";
                string alertMessage = "Usernamen and/or password incorrect!";
                ToastNotifier ToastNotifier = ToastNotificationManager.CreateToastNotifier();
                Windows.Data.Xml.Dom.XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
                Windows.Data.Xml.Dom.XmlNodeList toastNodeList = toastXml.GetElementsByTagName("text");
                toastNodeList.Item(0).AppendChild(toastXml.CreateTextNode(title));
                toastNodeList.Item(1).AppendChild(toastXml.CreateTextNode(alertMessage));
                Windows.Data.Xml.Dom.IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                Windows.Data.Xml.Dom.XmlElement audio = toastXml.CreateElement("audio");
                audio.SetAttribute("src", "ms-winsoundevent:Notification.SMS");

                ToastNotification toast = new ToastNotification(toastXml);
                toast.ExpirationTime = DateTime.Now.AddSeconds(4);
                ToastNotifier.Show(toast);

                Debug.WriteLine("Failed to authenticate!");
            }
        }

        //Facebook login
        private async void login_Click(object sender, RoutedEventArgs e)
        {
            FBSession clicnt = FBSession.ActiveSession;
            //clicnt.WinAppId = "s-1-15-2-346443637-505484848-1959051935-1085872266-2982689319-2567314515-3698788740";
            clicnt.WinAppId = "";
            clicnt.FBAppId = "1631620250464316";
            FBPermissions permissions = new FBPermissions(requested_permissions);
            FBResult result = await clicnt.LoginAsync(permissions);
            if (result.Succeeded)
            {
                Debug.WriteLine(result.Object);
                //Redirect to Main  screen
                this.Frame.Navigate(typeof(MainPage));
            }
            else
            {
                Debug.WriteLine(result);
            }
        }

        private void signup(object sender, RoutedEventArgs e)
        {
            //Redirect to Signup screen
            this.Frame.Navigate(typeof(SignupView));
        }
    }
}

