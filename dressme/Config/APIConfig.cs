﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dressme.Config
{
    class APIConfig
    {
        public static String URL = "https://dressmeapi.herokuapp.com/api"; //Oficial API Url
        //public static String URL = "http://localhost:8000/api"; //Used for Local Machine to access local API
        //public static String URL = "http://169.254.80.80:8000/api"; //Used for mobile simulator to access local API
        public static String URL_occasions = URL + "/occasions";
        public static String URL_upload_look = URL + "/upload/look";
        public static String URL_AMAZON_S3_bucket = "https://s3-us-west-2.amazonaws.com/dressmeapp/";
        public static String URL_DEFAULT_AUTHENTICATION = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCvdfTBL_rlqnaLZW8mfQYdjDVwq4-WOC8"; //Url to authenticate with default firebase authentication
        //public static String URL_STORAGE = "gs://dressme-84bf9.appspot.com";
    }
}
