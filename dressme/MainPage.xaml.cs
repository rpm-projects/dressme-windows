﻿using dressme.Config;
using dressme.Models;
using dressme.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace dressme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        ObservableCollection<OccasionModel> occasionList;        
        private OccasionModel selectedItem;

        public MainPage()
        {
            this.InitializeComponent();
            OccasionListView.ItemsSource = occasionList;
            loadOccasions();
        }

        //Load All Occasions
        private async void loadOccasions()
        {
         
            var rootFilter = new HttpBaseProtocolFilter();
            rootFilter.CacheControl.ReadBehavior = Windows.Web.Http.Filters.HttpCacheReadBehavior.MostRecent;
            rootFilter.CacheControl.WriteBehavior = Windows.Web.Http.Filters.HttpCacheWriteBehavior.NoCache;
            HttpClient client = new HttpClient(rootFilter);
            //client.DefaultRequestHeaders.Add("IfModifiedSince", Convert.ToString(DateTime.UtcNow));

            try
            {
                //Show loading
                LoadingIndicator.IsActive = true;

                this.occasionList = new ObservableCollection<OccasionModel>();
                OccasionModel occasion = null;

                //Get json values and parse it to occasion objects
                Uri geturi = new Uri(APIConfig.URL_occasions);
                HttpResponseMessage responseGet = await client.GetAsync(geturi);
                string jsonStr = await responseGet.Content.ReadAsStringAsync();

                Debug.WriteLine(jsonStr);

                var rootValue = JsonValue.Parse(jsonStr);

                //Debug.WriteLine(rootValue);

                var occasionsJson = rootValue.GetObject();
                foreach (var item in occasionsJson.ToList())
                {
                    occasion = new OccasionModel();
                    var obj = item.Value.GetObject();
                    occasion.id = item.Key ?? "";
                    occasion.name = obj.GetNamedString("name") ?? "";
                    occasion.desc = obj.GetNamedString("desc") ?? "";
                    //occasion.mood = Convert.ToInt16(obj.GetNamedNumber("mood"));
                    occasion.mood = 0;
                    occasion.location = obj.GetNamedString("location") ?? "";
                    occasion.date = obj.GetNamedString("date") ?? "";
                    occasion.time = obj.GetNamedString("time") ?? "";
                    occasion.url = obj.GetNamedString("url") ?? "";

                    var looks = obj.ContainsKey("looks") ? obj.GetNamedObject("looks") : null;

                    if (looks != null)
                    {
                        var looksJson = looks.ToList();
                        
                        foreach (var item2 in looksJson.ToList())
                        {
                            LookModel look = new LookModel();
                            var obj2 = item2.Value.GetObject();
                            look.id = item2.Key;
                            look.desc = obj2.GetNamedString("desc");
                            look.like = Convert.ToInt64(obj2.GetNamedNumber("like"));
                            look.dislike = Convert.ToInt64(obj2.GetNamedNumber("dislike"));
                            look.picture = obj2.GetNamedString("picture");
                            look.imageView = new Uri(APIConfig.URL_AMAZON_S3_bucket + "occasion_" + occasion.id + "_look_" + look.id);
                        
                            occasion.lookModels.Add(look);
                            //Used for list view
                            occasion.lookListForView.Add(look);
                        }
                    }

                    occasionList.Add(occasion);
                }

                OccasionListView.ItemsSource = occasionList;
            }
            finally
            {
                client.Dispose();
                //Hide loading 
                LoadingIndicator.IsActive = false;
                if(occasionList.Count <= 0)
                {
                    lbl_no_occasions.Visibility = Visibility.Visible;
                }
            }
        }

        private void Message_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OccasionListView.SelectedItem != null)
            {
                selectedItem = OccasionListView.SelectedItem as OccasionModel;
            }
        }

        //Redirect to Screen to Add new Occasion
        private void add_occasion(object sender, RoutedEventArgs e)
        {
            //Redirect to Setup Looks screen
            this.Frame.Navigate(typeof(AddOccasionView));
        }

        //Do logout
        private void logout(object sender, RoutedEventArgs e)
        {
            //Redirect to Login screen
            this.Frame.Navigate(typeof(LoginView));
        }

        //Refresh occasions list
        private void refreshOccasionsList(object sender, RoutedEventArgs e)
        {
            loadOccasions();
        }
    }
}
