﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace dressme.Models
{
    class LookModel
    {
        public String id { get; set; }
        public String desc { get; set; }
        public Int64 like { get; set; }
        public Int64 dislike { get; set; }
        public String picture { get; set; }
        public StorageFile storageFile { get; set;}
        public Uri imageView { get; set; }
        public String idForUpload { get; set; }
    }
}
