﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace dressme.Models
{
    class OccasionModel
    {
        public String id { get; set; }
        public String name { get; set; }
        public String desc { get; set; }
        public int mood { get; set; }
        public String location { get; set; }
        public String date { get; set; }
        public String time { get; set; }
        public String url { get; set; }
        public String picture { get; set; }
        public List<LookModel> lookModels { get; set; }
        public ObservableCollection<LookModel> lookListForView { get; set; }
        public int currentImg { get; set; }

        public OccasionModel()
        {
            this.lookModels = new List<LookModel>();
            this.lookListForView = new ObservableCollection<LookModel>();
        }
    }
}
